package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.repository.CountryRepository;

import org.springframework.data.domain.*;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.*;

import java.util.*;

import javax.persistence.criteria.CriteriaBuilder.In;

@RestController
public class CountryController {
	@Autowired
	private CountryRepository countryRepository;

	@CrossOrigin
	@PostMapping("/country/create")
	public ResponseEntity<Object> createCountry(@RequestBody CCountry cCountry) {
		try {
			CCountry newRole = new CCountry();
			newRole.setCountryName(cCountry.getCountryName());
			newRole.setCountryCode(cCountry.getCountryCode());
			newRole.setRegions(cCountry.getRegions());
			CCountry savedRole = countryRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
		}
	}

	@CrossOrigin
	@PutMapping("/country/update/{id}")
	public ResponseEntity<Object> updateCountry(@PathVariable("id") Long id, @RequestBody CCountry cCountry) {
		try {
			Optional<CCountry> countryData = countryRepository.findById(id);
			if (countryData.isPresent()) {
				CCountry newCountry = countryData.get();
				newCountry.setCountryName(cCountry.getCountryName());
				newCountry.setCountryCode(cCountry.getCountryCode());
				newCountry.setRegions(cCountry.getRegions());
				CCountry savedCountry = countryRepository.save(newCountry);
				return new ResponseEntity<>(savedCountry, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {

			return ResponseEntity.badRequest().body("Failed to get specified Country: " + id + "  for update.");
		}

	}

	@CrossOrigin
	@DeleteMapping("/country/delete/{id}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
		try {
			Optional<CCountry> optional = countryRepository.findById(id);
			if (optional.isPresent()) {
				countryRepository.deleteById(id);
			} else {
				// countryRepository.deleteById(id);
			}
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/country/details/{id}")
	public ResponseEntity<CCountry> getCountryById(@PathVariable Long id) {
		try {
			Optional<CCountry> countryData = countryRepository.findById(id);
			if (countryData.isPresent()) {
				return new ResponseEntity<>(countryData.get(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@CrossOrigin
	@GetMapping("/country/all")
	public ResponseEntity<List<CCountry>> getAllCountry() {
		try {
			List<CCountry> listcountry = new ArrayList<CCountry>();
			countryRepository.findAll().forEach(item -> {
				CCountry newlistCountry = new CCountry();
				newlistCountry.setCountryCode(item.getCountryCode());
				newlistCountry.setCountryName(item.getCountryName());
				newlistCountry.setRegions(item.getRegions());
				newlistCountry.setId(item.getId());
				newlistCountry.setRegionTotal(item.getRegions().size());
				listcountry.add(newlistCountry);
			});

			return new ResponseEntity<>(listcountry, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@GetMapping("/country5")
	public ResponseEntity<List<CCountry>> getFiveVoucher(
			@RequestParam(value = "page", defaultValue = "1") String page,
			@RequestParam(value = "size", defaultValue = "5") String size) {
		try {
			Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
			List<CCountry> list = new ArrayList<CCountry>();
			countryRepository.findAll(pageWithFiveElements).forEach(list::add);
			return new ResponseEntity<>(list, HttpStatus.OK);
		} catch (Exception e) {
			return null;
		}
	}
}
