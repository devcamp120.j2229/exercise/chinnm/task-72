package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.model.CRegion;
import com.devcamp.pizza365.repository.CountryRepository;
import com.devcamp.pizza365.repository.RegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class RegionController {
	@Autowired
	private RegionRepository regionRepository;

	@Autowired
	private CountryRepository countryRepository;

	@CrossOrigin
	@PostMapping("/region/create/{id}")
	public ResponseEntity<Object> createRegion(@PathVariable("id") Long id, @RequestBody CRegion cRegion) {
		try {
			Optional<CCountry> countryData = countryRepository.findById(id);
			if (countryData.isPresent()) {
				CRegion newRole = new CRegion();
				newRole.setRegionName(cRegion.getRegionName());
				newRole.setRegionCode(cRegion.getRegionCode());
				newRole.setCountry(cRegion.getCountry());

				CCountry _country = countryData.get();
				newRole.setCountry(_country);
				newRole.setCountryName(_country.getCountryName());

				CRegion savedRole = regionRepository.save(newRole);
				return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@CrossOrigin
	@PutMapping("/region/update/{id}")
	public ResponseEntity<Object> updateRegion(@PathVariable("id") Long id, @RequestBody CRegion cRegion) {
		try {
			Optional<CRegion> regionData = regionRepository.findById(id);
			if (regionData.isPresent()) {
				CRegion newRegion = regionData.get();
				newRegion.setRegionName(cRegion.getRegionName());
				newRegion.setRegionCode(cRegion.getRegionCode());
				CRegion savedRegion = regionRepository.save(newRegion);
				return new ResponseEntity<>(savedRegion, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			return ResponseEntity.badRequest().body("Failed to get specified region: " + id + "  for update.");
		}

	}

	@CrossOrigin
	@DeleteMapping("/region/delete/{id}")
	public ResponseEntity<Object> deleteRegionById(@PathVariable Long id) {
		try {
			regionRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/region/details/{id}")
	public ResponseEntity<CRegion> getRegionById(@PathVariable Long id) {
		try {
			Optional<CRegion> regionData = regionRepository.findById(id);
			if (regionData.isPresent()) {
				return new ResponseEntity<>(regionData.get(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@CrossOrigin
	@GetMapping("/region/all")
	public ResponseEntity<List<CRegion>> getAllRegion() {
		try {
			List<CRegion> regions = new ArrayList<CRegion>();
			regionRepository.findAll().forEach(regions::add);
			return new ResponseEntity<>(regions, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/country/{countryId}/regions")
	public ResponseEntity<List<CRegion>> getRegionsByCountry(@PathVariable(value = "countryId") Long countryId) {
		try {
			List<CRegion> regionData = regionRepository.findByCountryId(countryId);
			if (!regionData.isEmpty()) {
				return new ResponseEntity<>(regionData, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@CrossOrigin
	@GetMapping("/country/{countryId}/regions/{id}")
	public ResponseEntity<Optional<CRegion>> getRegionByRegionAndCountry(
			@PathVariable(value = "countryId") Long countryId,
			@PathVariable(value = "id") Long regionId) {
		try {
			Optional<CRegion> regionData = regionRepository.findByIdAndCountryId(countryId, regionId);
			if (regionData.isPresent()) {
				return new ResponseEntity<>(regionData, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}
}
