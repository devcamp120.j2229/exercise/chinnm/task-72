package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.model.CCustomer;
import com.devcamp.pizza365.model.COrder;
import com.devcamp.pizza365.repository.CustomerRepository;
import com.devcamp.pizza365.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/v1/order")

public class OrderController {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CustomerRepository uCuserRespository;

    @GetMapping("/all")
    public ResponseEntity<Object> getAllOrders() {
        List<COrder> orderList = new ArrayList<COrder>();
        try {
            orderRepository.findAll().forEach(orderElement -> {
                orderList.add(orderElement);
            });
            return new ResponseEntity<Object>(orderList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<Object> getOrderById(@PathVariable(name = "id") Long id) {
        Optional<COrder> _order = orderRepository.findById(id);
        if (_order.isPresent()) {
            return new ResponseEntity<Object>(_order, HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/customer/{customerId}/orders")
    public List<COrder> getRegionsByCountry(@PathVariable(value = "customerId") Long customerId) {
        return orderRepository.findByCustomerId(customerId);
    }

    @PostMapping("/create/{id}")
    public ResponseEntity<Object> createOrder(@PathVariable("id") Long id, @RequestBody COrder newOrde) {

        try {
            Optional<CCustomer> userData = uCuserRespository.findById(id);
            if (userData.isPresent()) {
                COrder _order = new COrder();
                _order.setCreated(new Date());
                _order.setUpdated(null);
                _order.setOrderCode(newOrde.getOrderCode());
                _order.setVoucherCode(newOrde.getVoucherCode());
                _order.setPizzaSize(newOrde.getPizzaSize());
                _order.setPizzaType(newOrde.getPizzaType());
                _order.setPrice(newOrde.getPrice());
                _order.setPaid(newOrde.getPaid());
                CCustomer _user = userData.get();
                _order.setCustomer(_user);
                COrder savedRole = orderRepository.save(_order);
                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }

        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable(name = "id") Long id, @RequestBody COrder orderUpdate) {
        Optional<COrder> _orderData = orderRepository.findById(id);
        if (_orderData.isPresent()) {
            COrder _order = _orderData.get();
            _order.setUpdated(new Date());
            _order.setPizzaSize(orderUpdate.getPizzaSize());
            _order.setPizzaType(orderUpdate.getPizzaType());
            _order.setPrice(orderUpdate.getPrice());
            _order.setPaid(orderUpdate.getPaid());
            _order.setVoucherCode(orderUpdate.getVoucherCode());
            _order.setOrderCode(orderUpdate.getOrderCode());
            try {
                return ResponseEntity.ok(orderRepository.save(_order));

            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Can not execute operation about this entity" + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteOrder(@PathVariable("id") Long id) {
        Optional<COrder> _orderData = orderRepository.findById(id);
        if (_orderData.isPresent()) {
            try {
                orderRepository.deleteById(id);
                return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);

            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Can not execute operation of this Entity" + e.getCause().getCause().getMessage());

            }
        } else {
            return new ResponseEntity<Object>("User not found", HttpStatus.NOT_FOUND);
        }
    }

}
