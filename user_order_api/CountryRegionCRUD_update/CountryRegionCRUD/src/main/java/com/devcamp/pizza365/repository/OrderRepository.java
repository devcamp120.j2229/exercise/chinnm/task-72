package com.devcamp.pizza365.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.pizza365.model.COrder;

@Repository
public interface OrderRepository extends JpaRepository<COrder, Long> {
	List<COrder> findByCustomerId(Long customerId);

}
