package com.devcamp.pizza365.repository;

import com.devcamp.pizza365.model.CCustomer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<CCustomer, Long> {
	// CCustomer findByCountryCode(String countryCode);

}
