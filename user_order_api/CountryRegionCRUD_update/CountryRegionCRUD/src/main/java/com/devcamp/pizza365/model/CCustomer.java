package com.devcamp.pizza365.model;

import javax.persistence.*;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "customer")
public class CCustomer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "fullname")
	private String fullname;
	@Column(name = "email")
	private String email;
	@Column(name = "phone")
	private String phone;
	@Column(name = "address")
	private String address;
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	@Column(name = "created_at", nullable = true, updatable = false)
	private Date created;
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	@Column(name = "updated_at", nullable = true, updatable = true)
	private Date updated;

	@OneToMany(targetEntity = COrder.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "customer_id")
	private List<COrder> regions;

	public CCustomer(String fullname, String email, String phone, String address) {
		this.fullname = fullname;
		this.email = email;
		this.phone = phone;
		this.address = address;
	}

	public CCustomer() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public List<COrder> getRegions() {
		return regions;
	}

	public void setRegions(List<COrder> regions) {
		this.regions = regions;
	}

}
