
$(document).ready(function () {
    onPageLoading();
    $('#user-table').on('click', 'tbody tr', function () {
        onRowClick(this);
    })
    $('#update-data-btn').on('click', function () {

        onBtnUpdateDataClick();
    })
    $('#save_data').on('click', function () {

        onBtnSaveDataClick();
    })
    $('#deleted-data-btn').on('click', function () {

        onBtnDeleteDataClick();
    })

    // $("#input-createDate").on("change", function () {
    // 	this.setAttribute(
    // 		"data-date",
    // 		moment(this.value, "DD/MM/YYYY")
    // 			.format(this.getAttribute("data-date-format"))
    // 	)

    // }).trigger("change")
    // $("#input-createDate").datepicker();
    // let date = $("#input-createDate").datepicker('getDate');
    // console.log('check date picker', date)
    // $("#input-updateDate").datepicker();


})
var table = $("#user-table").DataTable({
    "columns": [
        { "data": "id" },
        { "data": "fullname" },
        { "data": "email" },
        { "data": "phone" },
        { "data": "address" },
        { "data": "created" },
        { "data": "updated" },
    ]
})
var gRowSelected = ''
function onPageLoading() {

    $.ajax({
        url: "http://localhost:8080/v1/user/all",
        type: "GET",
        dataType: "json",
        success: function (res) {
            console.log('check data on tablw', res);
            table.clear();
            table.rows.add(res);
            table.draw();
        },
        error: function (err) {
            console.log(err.response);
        }
    })
}
function onRowClick(thisRow) {
    let rowSelected = table.row(thisRow).data();
    let dataId = rowSelected.id;

    callApiToGetVoucherById(dataId)
}
function callApiToGetVoucherById(id) {

    $.ajax({
        url: `http://localhost:8080/v1/user/detail/${id}`,
        async: false,
        type: "GET",
        dataType: "json",
        success: function (res) {
            console.log('check data ress', res);
            gRowSelected = res;
            console.log('check row selected', res)


            // table.clear();
            // table.rows.add(res);
            // table.draw();
            showDataOnForm();
        },
        error: function (err) {
            console.log(err.response);
        }
    })

}
function showDataOnForm() {
    $('#input-fullname').val(gRowSelected.fullname);
    $('#input-email').val(gRowSelected.email);
    $('#input-phone').val(gRowSelected.phone);
    $('#input-address').val(gRowSelected.address);


}
function onBtnUpdateDataClick() {
    let objData = {
        fullname: '',
        email: '',
        phone: '',
        address: '',


    }
    let objDataFromForm = getDataOnForm(objData);
    console.log('check row update', gRowSelected);
    console.log('check data update', objDataFromForm);

    callApiToUpdateData(gRowSelected.id, objDataFromForm);
    onPageLoading();

}
function getDataOnForm(obj) {
    obj.fullname = $('#input-fullname').val();
    obj.email = $('#input-email').val();
    obj.phone = $('#input-phone').val();
    obj.address = $('#input-address').val();

    return obj
}
function callApiToUpdateData(id, paramOrderObject) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/v1/user/update/${id}`,
        type: 'PUT',
        async: false,
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramOrderObject),
        success: function (res) {
            console.log('check data update response', res);

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}
function onBtnSaveDataClick() {
    let objData = {
        fullname: '',
        email: '',
        phone: '',
        address: '',


    }
    let objDataFromForm = getDataOnForm(objData);
    console.log('check data post', objDataFromForm)
    callApiToPostData(objDataFromForm);
    onPageLoading();
}
function callApiToPostData(paramOrderObject) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/v1/user/create`,
        type: 'POST',
        async: false,
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramOrderObject),
        success: function (res) {
            console.log('check data post response', res);

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}
function onBtnDeleteDataClick() {
    callApiToDeleteData(gRowSelected.id);
    onPageLoading();
}
function callApiToDeleteData(id) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/v1/user/delete/${id}`,
        type: 'DELETE',
        async: false,
        // contentType: "application/json;charset=UTF-8",
        // data: JSON.stringify(paramOrderObject),
        success: function (res) {
            console.log('Delete success')

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}


