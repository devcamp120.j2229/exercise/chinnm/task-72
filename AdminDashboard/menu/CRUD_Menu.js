
$(document).ready(function () {
    onPageLoading();
    $('#user-table').on('click', 'tbody tr', function () {
        onRowClick(this);
    })
    $('#update-data-btn').on('click', function () {

        onBtnUpdateDataClick();
    })
    $('#save_data').on('click', function () {

        onBtnSaveDataClick();
    })
    $('#deleted-data-btn').on('click', function () {

        onBtnDeleteDataClick();
    })
    $('#deleted-all-data-btn').on('click', function () {

        onBtnDeleteAllDataClick();
    })
    // $("#input-createDate").on("change", function () {
    // 	this.setAttribute(
    // 		"data-date",
    // 		moment(this.value, "DD/MM/YYYY")
    // 			.format(this.getAttribute("data-date-format"))
    // 	)

    // }).trigger("change")
    // $("#input-createDate").datepicker();
    // let date = $("#input-createDate").datepicker('getDate');
    // console.log('check date picker', date)
    // $("#input-updateDate").datepicker();


})
var table = $("#user-table").DataTable({
    "columns": [
        { "data": "id" },
        { "data": "size" },
        { "data": "duongKinh" },
        { "data": "suon" },
        { "data": "salad" },
        { "data": "soLuongNuocNgot" },
        { "data": "donGia" },
        { "data": "ngayTao" },
        { "data": "ngayCapNhat" },
    ]
})
var gRowSelected = ''
function onPageLoading() {

    $.ajax({
        url: "http://localhost:8080/all-menu",
        type: "GET",
        dataType: "json",
        success: function (res) {
            console.log('check data on tablw', res);
            table.clear();
            table.rows.add(res);
            table.draw();
        },
        error: function (err) {
            console.log(err.response);
        }
    })
}
function onRowClick(thisRow) {
    let rowSelected = table.row(thisRow).data();
    let dataId = rowSelected.id;

    callApiToGetVoucherById(dataId)
}
function callApiToGetVoucherById(id) {

    $.ajax({
        url: `http://localhost:8080/menu/${id}`,
        async: false,
        type: "GET",
        dataType: "json",
        success: function (res) {
            console.log('check data ress', res);
            gRowSelected = res;
            console.log('check row selected', res)


            // table.clear();
            // table.rows.add(res);
            // table.draw();
            showDataOnForm();
        },
        error: function (err) {
            console.log(err.response);
        }
    })

}
function showDataOnForm() {
    $('#input-size').val(gRowSelected.size);
    $('#input-duong-kinh').val(gRowSelected.duongKinh);
    $('#input-suon').val(gRowSelected.suon);
    $('#input-salad').val(gRowSelected.salad);
    $('#input-so-luong-nuoc').val(gRowSelected.soLuongNuocNgot);
    $('#input-don-gia').val(gRowSelected.donGia);


}
function onBtnUpdateDataClick() {
    let objData = {
        size: '',
        duongKinh: 0,
        suon: 0,
        salad: 0,
        soLuongNuocNgot: 0,
        donGia: 0


    }
    let objDataFromForm = getDataOnForm(objData);
    console.log('check row update', gRowSelected);
    console.log('check data update', objDataFromForm);

    callApiToUpdateData(gRowSelected.id, objDataFromForm);
    onPageLoading();

}
function getDataOnForm(obj) {
    obj.size = $('#input-size').val();
    obj.duongKinh = $('#input-duong-kinh').val();
    obj.donGia = $('#input-don-gia').val();
    obj.suon = $('#input-suon').val();
    obj.salad = $('#input-salad').val();
    obj.soLuongNuocNgot = $('#input-so-luong-nuoc').val();


    return obj
}
function callApiToUpdateData(id, paramOrderObject) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/menu/${id}`,
        type: 'PUT',
        async: false,
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramOrderObject),
        success: function (res) {
            console.log('check data update response', res);

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}
function onBtnSaveDataClick() {
    let objData = {
        size: '',
        duongKinh: 0,
        suon: 0,
        salad: 0,
        soLuongNuocNgot: 0,
        donGia: 0


    }
    let objDataFromForm = getDataOnForm(objData);
    console.log('check data post', objDataFromForm)
    callApiToPostData(objDataFromForm);
    onPageLoading();
}
function callApiToPostData(paramOrderObject) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/menu`,
        type: 'POST',
        async: false,
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramOrderObject),
        success: function (res) {
            console.log('check data post response', res);

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}
function onBtnDeleteDataClick() {
    callApiToDeleteData(gRowSelected.id);
    onPageLoading();
}
function callApiToDeleteData(id) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/menu/${id}`,
        type: 'DELETE',
        async: false,
        // contentType: "application/json;charset=UTF-8",
        // data: JSON.stringify(paramOrderObject),
        success: function (res) {
            console.log('Delete success')

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}
function onBtnDeleteAllDataClick() {
    $.ajax({
        async: false,
        url: `http://localhost:8080/menus`,
        type: 'DELETE',
        async: false,
        // contentType: "application/json;charset=UTF-8",
        // data: JSON.stringify(paramOrderObject),
        success: function (res) {
            console.log('Delete all success')

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}

